package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
	
	@Override
	public void doGet (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException { 
		resp.setHeader("Content-Type", "text/plain");
		resp.getWriter().println("Hello World");
		
		
	}

}
