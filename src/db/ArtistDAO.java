package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Artist;

public class ArtistDAO {
	
	private final ChinookDataBase db;
	
	public ArtistDAO() {
		db = new ChinookDataBase();
	}
	public Artist findArtist(long id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
	try {
		connection = db.connect();
		statement = connection.prepareStatement("SELECT * FROM Artist WHERE ArtistId = ?");
		statement.setLong(1, id);
		results = statement.executeQuery();
		
		if(results.next()) {
			String name = results.getString("Name");
			long ArtistId = results.getLong("ArtistId");
			return new Artist(ArtistId, name);
		}
	} catch (Exception e) {
		throw new RuntimeException(e);
	} finally {
		db.closeAll(connection, statement, results);
	} 
		return null;
	}
	public List<Artist> findAllArtists()  {
		List<Artist> allArtists = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		
	try {
		 connection = db.connect();
		 statement = connection.prepareStatement("SELECT * FROM Artist");
		 results = statement.executeQuery();
		
		while (results.next()) { 
			long id = results.getLong("ArtistId");
			String name = results.getString("Name");
			
			Artist a = new Artist(id, name);
			allArtists.add(a);
		}
	
	}	catch (Exception e) {
		throw new RuntimeException(e);
		
	}	finally {
		db.closeAll(connection, statement, results);
	}
		
		return allArtists;
	}
}

