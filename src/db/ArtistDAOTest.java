package db;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import models.Artist;

public class ArtistDAOTest {

	private ArtistDAO dao = new ArtistDAO();
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testFirstArtist()  {
		List<Artist> artists = dao.findAllArtists();
		Artist first = artists.get(0);
		
		assertEquals("AC/DC", first.getName());
	}
	@Test
	public void testSecondArtist()  {
		List<Artist> artists = dao.findAllArtists();
		Artist second = artists.get(1);
		
		assertEquals("Accept", second.getName());
	}

}
