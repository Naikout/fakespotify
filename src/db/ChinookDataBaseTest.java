package db;


import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.sql.Connection;

import org.junit.Before;
import org.junit.Test;

public class ChinookDataBaseTest {
	
	private ChinookDataBase database;

	@Before
	public void setUp() throws Exception {
		database = new ChinookDataBase();
	}

	@Test
	public void testOpeningConnection() throws ClassNotFoundException, SQLException {
		Connection connection = database.connect();
		assertNotNull(connection);
	}
	
}
